const path = require('path');

const reduce = (obj, filename) => {
    return Object.keys(obj).reduce((objects, key) => {
        if (new RegExp(filename).test(key)) {
            return [...objects, obj[key]];
        } else if (typeof obj[key] === 'object') {
            return [...objects, ...reduce(obj[key], filename)];
        }

        return objects;
    }, []);
};

const combine = objects =>
    objects.reduce((final, current) => Object.assign({}, final, current), {});

const nullIfEmptyObject = obj => {
    if (typeof obj === 'object') {
        for (let key in obj) {
            if (hasOwnProperty.call(obj, key)) {
                return obj;
            }
        }
    }

    return null;
};

const getLocalization = lang => {
    const obj = require('require-all')({
        dirname: path.resolve('./src'),
        filter: new RegExp(`^[a-zA-Z0-9\-_\.]+.${lang}.json$`, 'i'),
        recursive: true,
    });
    return nullIfEmptyObject(combine(reduce(obj, `${lang}.json`)));
};

module.exports = {
    getLocalization,
};
