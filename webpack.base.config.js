const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const pathsToClean = ['dist', 'build'];
const cleanOptions = {
    root: __dirname,
    exclude: [],
    verbose: true,
    dry: false,
};

module.exports = {
    devtool: 'eval-cheap-source-emap',
    entry: ['babel-polyfill', path.resolve('./src/index.tsx')],
    output: {
        path: path.resolve('./dist'),
        publicPath: '/',
        filename: `[name].bundle.js`,
    },
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ['babel-loader', 'awesome-typescript-loader'],
            },
            {
                test: /\.css$/,
                include: [path.join(__dirname, 'src', 'styles')],
                loaders: ['style-loader', 'css-loader', 'postcss-loader'],
            },
            {
                test: /\.less$/,
                loaders: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName:
                                '[path]___[name]__[local]___[hash:base64:5]',
                        },
                    },
                    'less-loader',
                ],
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff',
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader',
            },
            {
                test: /\.css$/,
                exclude: [path.join(__dirname, 'src', 'styles')],
                loaders: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName:
                                '[path]___[name]__[local]___[hash:base64:5]',
                        },
                    },
                    'postcss-loader',
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json', '.css', '.png', '.jpg'],
        modules: ['src', 'node_modules'],
    },
};
