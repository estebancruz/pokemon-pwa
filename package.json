{
  "name": "pokemon-pwa",
  "description": "Pokemon Progressive Web App",
  "version": "1.0.0",
  "main": "server.js",
  "repository": "git@bitbucket.org:drew.wyatt@bottlerocketstudios.com/thin-air-web.git",
  "author": "🚀 The Bottle Rocket Web Team 🚀",
  "contributors": [
    "Esteban De La Cruz <esteban.cruz@bottlerocketstudios.com>",
    "Drew Wyatt <drew.wyatt@bottlerocketstudios.com>"
  ],
  "license": "UNLICENSED",
  "scripts": {
    "build": "webpack -p --config=webpack.prod.config.js",
    "build-storybook": "build-storybook -c .storybook -o styleguide",
    "compile": "tsc",
    "coverage": "jest --coverage --no-cache",
    "document": "typedoc --readme ./README.md --out docs ./src/",
    "document:force-local-compiler": "rimraf node_modules/typedoc/node_modules/typescript",
    "lint": "tslint './src/**/*.ts*' --fix",
    "postinstall": "npm run document:force-local-compiler",
    "precommit": "lint-staged",
    "prettify": "npm run prettify:ts && npm run prettify:less && npm run prettify:js",
    "prettify:js": "prettier --write --tab-width=4 --single-quote --trailing-comma=all \"./**/*.js\"",
    "prettify:less": "prettier --write --tab-width=4 \"./src/**/*.less\"",
    "prettify:ts": "prettier --write --single-quote --tab-width=4 --parser=typescript --trailing-comma=all \"./src/**/*.ts*\"",
    "start-mock-server": "node ./mocks/index.js",
    "start": "node server.js",
    "storybook": "start-storybook -p 9090 -c .storybook",
    "test": "jest --no-cache"
  },
  "dependencies": {
    "@types/jest": "^19.2.4",
    "@types/node": "^8.0.14",
    "@types/react": "^15.0.27",
    "@types/react-css-modules": "^4.2.0",
    "@types/react-dom": "^15.5.0",
    "@types/react-hot-loader": "^3.0.1",
    "@types/react-router-dom": "^4.0.4",
    "@types/react-router-redux": "^5.0.2",
    "@types/redux-logger": "^3.0.0",
    "awesome-typescript-loader": "^3.1.3",
    "babel-core": "^6.25.0",
    "babel-jest": "^20.0.3",
    "babel-loader": "^7.0.0",
    "babel-polyfill": "^6.23.0",
    "babel-preset-es2015": "^6.24.1",
    "babel-preset-react": "^6.24.1",
    "bootstrap": "^3.3.7",
    "caniuse-db": "^1.0.30000697",
    "css-loader": "^0.28.4",
    "firebase": "^4.1.3",
    "google-libphonenumber": "^2.0.18",
    "history": "^4.6.2",
    "html-webpack-plugin": "^2.28.0",
    "husky": "^0.14.3",
    "i18n-webpack-plugin": "^1.0.0",
    "identity-obj-proxy": "^3.0.0",
    "jest": "^20.0.4",
    "jest-cli": "^20.0.4",
    "jest-enzyme": "^3.2.0",
    "jquery": "^3.2.1",
    "less": "^2.7.2",
    "less-loader": "^4.0.4",
    "lint-staged": "^4.0.1",
    "postcss-cssnext": "^2.11.0",
    "postcss-import": "^10.0.0",
    "postcss-loader": "^2.0.6",
    "react": "^15.6.1",
    "react-css-modules": "^4.3.0",
    "react-dom": "^15.6.0",
    "react-hot-loader": "^3.0.0-beta.6",
    "react-redux": "^5.0.5",
    "react-router-dom": "^4.1.1",
    "react-router-redux": "^4.0.8",
    "redux": "^3.6.0",
    "redux-logger": "^3.0.6",
    "redux-saga": "^0.15.3",
    "regenerator-runtime": "^0.10.5",
    "require-all": "^2.2.0",
    "rimraf": "^2.6.1",
    "style-loader": "^0.18.2",
    "ts-jest": "^20.0.6",
    "tslint-immutable": "^4.0.1",
    "typedoc": "^0.7.1",
    "typescript": "2.4",
    "webpack": "^2.6.1",
    "webpack-dev-server": "^2.4.5"
  },
  "jest": {
    "collectCoverageFrom": [
      "src/**/*.{ts,tsx}",
      "!src/**/*.story.{ts,tsx}",
      "!src/**/*.test.{js,jsx,ts,tsx}",
      "!src/**/index.{js,jsx,ts,tsx}"
    ],
    "coverageThreshold": {
      "global": {
        "statements": 40,
        "branches": 40,
        "functions": 40,
        "lines": 80
      }
    },
    "moduleDirectories": [
      "node_modules",
      "src"
    ],
    "transform": {
      "^.+\\.tsx?$": "<rootDir>/jest/jest.preprocessor.js"
    },
    "moduleNameMapper": {
      "\\.(css|less)$": "identity-obj-proxy"
    },
    "testRegex": "(/__tests__/.*|(.|/)(test|spec)).tsx?$",
    "moduleFileExtensions": [
      "ts",
      "tsx",
      "js",
      "json"
    ],
    "setupFiles": [
      "<rootDir>/jest/mock-i18n.js"
    ]
  },
  "devDependencies": {
    "@kadira/storybook": "^2.35.3",
    "@kadira/storybook-ui": "^3.11.0",
    "clean-webpack-plugin": "^0.1.16",
    "deepfreeze": "^2.0.0",
    "enzyme": "^2.8.2",
    "fs": "0.0.1-security",
    "hock": "^1.3.2",
    "http": "0.0.0",
    "path": "^0.12.7",
    "prettier": "^1.5.2",
    "react-addons-test-utils": "^15.6.0",
    "react-test-renderer": "^15.6.1",
    "redux-mock-store": "^1.2.3",
    "redux-saga-test-plan": "^3.1.0",
    "request": "^2.81.0",
    "tslint": "^5.4.3",
    "tslint-react": "^3.0.0"
  }
}
