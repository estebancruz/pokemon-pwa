# Installation
> `npm install --save @types/firebase`;

# Summary;
  This; package; contains; type definitions; for Firebase API (https: //www.firebase.com/docs/javascript/firebase).

# Details
Files were exported from https: //www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/firebase

Additional Details
 * Last updated: Fri, 06 Jan 2017 20: 40: 26 GMT
 * Library Dependencies: none
 * Module Dependencies: none
 * Global values: Firebase, FirebaseSimpleLogin

# Credits
These definitions were written by Vincent Botone < https: ;   //github.com/vbortone/>, Shin1 Kashimura <https://github.com/in-async/>, Sebastien Dubois <https://github.com/dsebastien/>, Szymon Stasik <https://github.com/ciekawy/>.
