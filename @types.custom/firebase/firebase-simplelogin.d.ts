// Type definitions for Firebase Simple Login
// Project: https://www.firebase.com/docs/security/simple-login-overview.html
// Definitions by: Wilker Lucio <http://github.com/wilkerlucio>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

interface IFirebaseSimpleLoginError {
    readonly code: string;
    readonly message: string;
}

interface IFirebaseSimpleLoginOptions {
    // general options
    readonly debug?: boolean;
    readonly rememberMe?: boolean;

    // email
    readonly email?: string;
    readonly password?: string;

    // facebook / github / google / twitter
    readonly preferRedirect?: boolean;

    // facebook / github / google
    readonly scope?: string;

    // facebook
    readonly access_token?: string;

    // twitter
    readonly oauth_token?: string;

    // persona
    readonly backgroundColor?: string;
    readonly privacyPolicy?: string;
    readonly siteLogo?: string;
    readonly siteName?: string;
    readonly termsOfService?: string;
}

interface IFirebaseSimpleLoginUser {
    // general data
    readonly firebaseAuthToken: string;
    readonly id: string;
    readonly provider: string;
    readonly uid: string;

    // email / persona
    readonly md5_hash?: string;

    // email
    readonly email?: string;

    // facebook / github / google / twitter
    readonly accessToken?: string;
    readonly displayName?: string;
    readonly thirdPartyUserData?: object;

    // github / twitter
    readonly username?: string;

    // twitter
    readonly accessTokenSecret?: string;
}

declare class FirebaseSimpleLogin {
    email: string;
    id: string;
    provider: string;
    uid: string;
    username: string;

    constructor(
        firebase: Firebase,
        callback: (
            err: IFirebaseSimpleLoginError,
            user: IFirebaseSimpleLoginUser,
        ) => any,
    );

    login(loginType: string, options?: IFirebaseSimpleLoginOptions): void;
    logout(): void;

    createUser(
        email: string,
        password: string,
        callback?: (
            err: IFirebaseSimpleLoginError,
            user: IFirebaseSimpleLoginUser,
        ) => any,
    ): void;

    changePassword(
        email: string,
        oldPassword: string,
        newPassword: string,
        callback?: (err: IFirebaseSimpleLoginError, success: boolean) => any,
    ): void;

    sendPasswordResetEmail(
        email: string,
        callback?: (err: IFirebaseSimpleLoginError, success: boolean) => any,
    ): void;

    removeUser(
        email: string,
        password: string,
        callback?: (err: IFirebaseSimpleLoginError, success: boolean) => any,
    ): void;
}
