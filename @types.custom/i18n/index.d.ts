/**
 * Used by i18n-webpack-plugin to check for internationalizations
 * @param label The string to (maybe) replace
 */
declare function __(label: string): string;
