const hock = require('hock');
const http = require('http');
const request = require('request');
const path = require('path');
const fs = require('fs');

//-------------------------------------
// settings
//-------------------------------------
const HOST = 'localhost';
const PORT = 8080;
const MOCK_FOLDER_LOCATION = path.join(__dirname, './services');
const MOCK_DATA_CONFIG_FILE_PER_FOLDER = 'data.json';
const endpoints = [];

//-------------------------------------
// activate - starting point
//-------------------------------------
activate();

/**
 * Add default mocks that may be necessary for typical development here.
 *
 * @param mock
 */
function createDefaultMocks(mock) {
    mock.get('/favicon.ico').any().reply(200, '');
    // general list of our endpoints
    let endpointsString =
        'The following endpoints are available to you: ' +
        '\n' +
        'METHOD' +
        '\t\t' +
        'ROUTE' +
        '\t\t' +
        'EXPRESSION' +
        '\n';
    for (let i = 0, n = endpoints.length; i < n; i++) {
        endpointsString = endpointsString + endpoints[i] + '\n';
    }
    mock.get('/').any().reply(200, endpointsString);
    // Add your own default mocks that might be necessary
}

/**
 * Main starting point for server and pulling necessary data.
 */
function activate() {
    let folders = getListOfFolders();
    let mock = hock.createHock();
    for (let i = 0, n = folders.length; i < n; i++) {
        let folder = folders[i];
        let jsonArray = getJsonDataForFolder(folder);
        console.log('Processing folder: ' + folder);
        createMockByJsonArray(mock, jsonArray, folder);
    }
    createDefaultMocks(mock);
    startServer(mock);
}

/**
 * Uses the loaded json data by folder and loops
 * through to create each mock.
 *
 * @param mock
 * @param jsonArray
 * @param folder
 * @returns {null}
 */
function createMockByJsonArray(mock, jsonArray, folder) {
    if (!jsonArray || jsonArray.length === 0) {
        return null;
    }
    for (let i = 0, n = jsonArray.length; i < n; i++) {
        if (!jsonArray[i] || !jsonArray[i].route) {
            continue;
        }
        jsonArray[i].method = jsonArray[i].method || 'GET';
        console.log(
            'Adding mock: ' + jsonArray[i].method + ' ' + jsonArray[i].route,
        );
        let endpoint = jsonArray[i].method + '\t\t' + jsonArray[i].route;
        if (jsonArray[i].regexp && jsonArray[i].regexpReplace) {
            endpoint = endpoint + '\t' + jsonArray[i].regexp;
        }
        endpoints.push(endpoint);
        createMock(mock, jsonArray[i], folder);
    }
}

/**
 * Uses the data provided by each json element to
 * create the necessary mock data.
 *
 * @param mock
 * @param value
 * @param folder
 * @returns {*}
 */
function createMock(mock, value, folder) {
    let request = null;
    // null checks
    value.response = value.response || {
        statusCode: 200,
        headers: null,
        body: 'ok',
        file: null,
    };
    // regexp replace for urls
    if (value.regexp && value.regexpReplace) {
        let regexp = new RegExp(value.regexp, 'gi');
        request = mock.filteringPathRegEx(regexp, value.regexpReplace);
    }
    // method and route
    switch (value.method) {
        case 'POST':
            request = request
                ? request.post(value.route)
                : mock.post(value.route);
            break;
        case 'PUT':
            request = request
                ? request.put(value.route)
                : mock.put(value.route);
            break;
        case 'PATCH':
            request = request
                ? request.patch(value.route)
                : mock.patch(value.route);
            break;
        case 'DESTROY':
            request = request
                ? request.delete(value.route)
                : mock.delete(value.route);
            break;
        case 'COPY':
            request = request
                ? request.copy(value.route)
                : mock.copy(value.route);
            break;
        case 'HEAD':
            request = request
                ? request.head(value.route)
                : mock.head(value.route);
            break;
        case 'GET':
        default:
            request = request
                ? request.get(value.route)
                : mock.get(value.route);
            break;
    }
    // min/max quantity
    if ((value.min && value.min > 0) || (value.max && value.max > 0)) {
        request.many({
            min: value.min,
            max: value.max,
        });
    } else {
        request.any();
    }
    // response
    if (value.response.file) {
        request.replyWithFile(
            value.response.statusCode,
            path.join(folder, value.response.file),
            value.response.headers,
        );
    } else {
        request.reply(
            value.response.statusCode,
            value.response.body,
            value.response.headers,
        );
    }
    // done
    return request;
}

/**
 * Starts the mock server. It might be nice later on to
 * add some logging here but it isn't necessary.
 *
 * @param mock
 */
function startServer(mock) {
    let server = http.createServer(mock.handler);
    server.listen(
        {
            host: HOST,
            port: PORT,
        },
        error => {
            if (error) {
                console.log(error);
                //throw error;
            }
            console.log('Mock Server running at:', server.address());
        },
    );
}

/**
 * Pulls the json data by folder.
 *
 * @param value
 */
function getJsonDataForFolder(value) {
    let contents = fs.readFileSync(
        path.join(value, MOCK_DATA_CONFIG_FILE_PER_FOLDER),
    );
    return JSON.parse(contents);
}

/**
 * Pulls a list of all the folders necessary for mocking data.
 *
 * @returns {Array}
 */
function getListOfFolders() {
    let folders = [];
    let files = fs.readdirSync(MOCK_FOLDER_LOCATION);
    files
        .map(file => {
            return path.join(MOCK_FOLDER_LOCATION, file);
        })
        .filter(file => {
            return fs.statSync(file).isDirectory();
        })
        .forEach(file => {
            folders.push(file);
        });
    return folders;
}
