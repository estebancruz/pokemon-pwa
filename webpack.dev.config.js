/**
 * Makes sure that the internationalization function (defaults to "__")
 * Is not undefined, but still only splits out one configuration 
 */

const webpack = require('webpack');
const path = require('path');
const I18nPlugin = require('i18n-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const baseConfig = require('./webpack.base.config');
const { getLocalization } = require('./i18n/configure');

const languageArg = process.argv.find(a => a.indexOf('--language=') > -1);
const language = languageArg ? languageArg.split('=')[1] : 'en';
const config = Object.assign({}, baseConfig, {
    entry: [
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        'babel-polyfill',
        path.resolve('./src/index.tsx'),
    ],
    plugins: baseConfig.plugins.concat([
        new HtmlWebpackPlugin({
            template: path.resolve('./src/index.html'),
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new I18nPlugin(getLocalization(language)),
    ]),
});

module.exports = config;
