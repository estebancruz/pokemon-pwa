FROM node:7.7.2

RUN useradd jenkins --uid 498 --shell /bin/bash --create-home
RUN mkdir /.npm; chown jenkins /.npm && mkdir /.prep-tmp; chown jenkins /.prep-tmp
USER jenkins