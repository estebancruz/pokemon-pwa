import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import Welcome from './welcome';

declare var module: any;

storiesOf('Welcome', module).add('to Storybook', () => <Welcome />);
