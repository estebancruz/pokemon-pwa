/**
 * Creates separate bundles for each language specified on line 8.
 */

const path = require('path');
const baseConfig = require('./webpack.base.config');
const I18nPlugin = require('i18n-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { getLocalization } = require('./i18n/configure');
const languages = ['en', 'de'];

const configForLanguage = language =>
    Object.assign({}, baseConfig, {
        output: Object.assign({}, baseConfig.output, {
            filename: `[name].${language}.js`,
        }),
        plugins: baseConfig.plugins.concat(baseConfig.plugins, [
            new I18nPlugin(getLocalization(language)),
            new HtmlWebpackPlugin({
                template: path.resolve('./src/index.html'),
                filename: `index.${language}.html`,
            }),
        ]),
    });

module.exports = languages.map(configForLanguage);
