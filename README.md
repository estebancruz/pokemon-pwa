# Base React project 


### (used as a basis for our internal Yo React Generator)

[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

---


## Setup

**Before you start**

You will need:

* [Node](https://nodejs.org/en/) (recommended version: 7.7.2)
* [The Yarn Package Manager](https://yarnpkg.com/en/)


Then:

    $ yarn install



---


## Scripts


**Most Common**


### Start webpack dev server


    $ yarn start


### Compile and bundle the application for deployment


    $ yarn build


### Run unit tests with [Jest](https://facebook.github.io/jest/)


    $ yarn test


To run unit tests and generate a coverage report:


    $ yarn coverage


### Generate documentation with [TypeDoc](http://typedoc.org/)


    $ yarn document


### Lint using [TSLint](https://palantir.github.io/tslint/)


    $ yarn lint


### Format with [Prettier](https://github.com/prettier/prettier)


    $ yarn prettify


### Start mock API server via [Hock](https://github.com/mmalecki/hock)


    $ yarn start-mock-server


### Start local styleguide server (for testing components in isolation or viewing the styleguide) via [Storybook](https://storybook.js.org/)


    $ yarn storybook


---


**Hooks**


### Post-Install


    $ yarn postinstall



### Pre-Commit via [lint-staged](https://github.com/okonet/lint-staged)


    $ yarn precommit


---


**Everything else (alphabetical)**



### Run the TypeScript compiler


    $ yarn compile


### Force TypeDoc to use this project's TypeScript compiler 


(deletes `/node_modules/typedoc/node_modules/typescript`)


    $ yarn document:force-local-compiler


### Run prettier on JS files


    $ yarn run prettify:js


### Run prettier on Less files


    $ yarn run prettify:less


### Run prettier on TS files


    $ yarn run prettify:ts


