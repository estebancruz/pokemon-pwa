import { JsonService } from 'utils';

class PokemonService extends JsonService {
    private static BASE_URL = 'https://pokeapi.co/api/v2/';

    static async fetchGeneration(gen: number): Promise<any> {
        return PokemonService.fetchJson(
            PokemonService.BASE_URL + `generation/${gen}`,
        );
    }

    static async fetchPokemon(id: number): Promise<any> {
        return PokemonService.fetchJson(
            PokemonService.BASE_URL + `pokemon/${id}`,
        );
    }
}

export default PokemonService;
