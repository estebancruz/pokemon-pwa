// tslint:disable-next-line:no-reference
/// <reference path="../@types.custom/index.d.ts" />
import * as React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { configureStore } from 'states';
import Root from 'views/root';

const app = document.getElementById('app');
const store = configureStore();
render(
    <AppContainer>
        <Root store={store} />
    </AppContainer>,
    app,
);

if ((module as any).hot) {
    (module as any).hot.accept('./views/root', () => {
        const RootContainer = require('./views/root').default;
        render(
            <AppContainer>
                <RootContainer store={store} />
            </AppContainer>,
            app,
        );
    });
}
