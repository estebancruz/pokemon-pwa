type SplitProps<Props extends {}> = [Partial<Props>, Partial<Props>];
const splitProps = <Props extends {}>(
    props: Props,
    legend: {},
): SplitProps<Props> => {
    return Object.keys(props).reduce((tuple, key) => {
        const [legendProps, otherProps] = tuple;
        const sp: SplitProps<Props> =
            legend[key] && (legend as any)[key].length
                ? [{ ...legendProps as {}, [key]: props[key] }, otherProps]
                : [legendProps, { ...otherProps as {}, [key]: props[key] }];
        return sp;
    }, [{}, {}] as SplitProps<Props>);
};

export default splitProps;
