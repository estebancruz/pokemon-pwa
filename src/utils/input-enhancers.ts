import { PhoneNumberFormat, PhoneNumberUtil } from 'google-libphonenumber';

export type InputEnhancer = (value: string) => string;
export interface EnhancerObject {
    readonly validators: ReadonlyArray<InputEnhancer>;
    readonly formatters: ReadonlyArray<InputEnhancer>;
}
export namespace InputEnhancers {
    const phoneUtil = PhoneNumberUtil.getInstance();

    export namespace Validators {
        /**
         * Uses google-libphonenumber to determine if this is a valid US phone number.
         * Returns an error if input is invalid.
         */
        export const isPhoneNumber = (val: string): string => {
            if (!val || !val.trim()) {
                return '';
            }

            try {
                if (!!phoneUtil.parse(val, 'US') && val.length >= 10) {
                    return '';
                } else {
                    return 'Please enter a valid phone number.';
                }
            } catch (e) {
                return 'Please enter a valid phone number.';
            }
        };

        /**
         * Returns an error if input is empty,
         */
        export const required = (
            val: string | ReadonlyArray<string> | Set<string>,
        ): string => {
            const error = 'This field cannot be left blank.';
            if (val && typeof (val as Set<any>).size !== 'undefined') {
                return (val as Set<any>).size > 0 ? '' : error;
            }

            try {
                const length =
                    val && (val as string).trim
                        ? (val as string).trim().length
                        : (val as ReadonlyArray<string>).length;
                return length > 0 ? '' : error;
            } catch (err) {
                // tslint:disable-next-line:no-console
                console.error(err);
            }

            return error;
        };

        /**
         * FACTORY: Returns an error if input is empty AND factory condition is met.,
         */
        export const requiredIf = (
            condition: () => boolean,
            errorMsg: string = 'This field cannot be left blank.',
        ): ((val: string) => string) => {
            return (val: string): string => {
                try {
                    return condition() && (!val || !val.trim()) ? errorMsg : '';
                } catch (err) {
                    return '';
                }
            };
        };

        /**
         * FACTORY: Returns an error if input is not exactly the provided length
         */
        export const exactLength = (length: number) => (
            val: string | ReadonlyArray<string> | Set<string>,
        ) => {
            const error =
                typeof val === 'object'
                    ? `Please make exactly ${length} selections.`
                    : `Input must be exactly ${length} characters.`;

            if (typeof (val as Set<any>).size !== 'undefined') {
                return (val as Set<any>).size === length ? '' : error;
            }

            return (val as string).length === length ? '' : error;
        };

        /**
         * FACTORY: Returns an error if the string, array, or set length is not at least the specified length.
         */
        export const lengthAtLeast = (length: number) => (
            val: string | ReadonlyArray<string> | Set<string>,
        ) => {
            const error =
                typeof val === 'object'
                    ? `Please make at least ${length} selections.`
                    : `Input must be at least ${length} characters.`;

            if (typeof (val as Set<any>).size !== 'undefined') {
                return (val as Set<any>).size >= length ? '' : error;
            }

            return (val as string).length >= length ? '' : error;
        };

        /**
         * Returns an error if input contains non-numeric characters
         */
        export const numeric = (val: string): string =>
            !val || !val.trim() || /(?:\d*\.)?\d+/.exec(val)
                ? ''
                : 'Only numbers are accepted.';

        /**
         * FACTORY: Returns an error if input is outside of provided range.
         */
        export const inNumericRange = (
            low: number,
            high: number,
            inclusive: boolean = false,
        ): ((val: string) => string) => {
            return (val: string) => {
                const error = `Value must be between ${low} and ${high}.`;
                try {
                    const n = Number(val);
                    const condition = inclusive
                        ? low <= n && n <= high
                        : low < n && n < high;
                    return condition ? '' : error;
                } catch (err) {
                    return error;
                }
            };
        };

        /**
         * FACTORY: Returns an error if input is more precise than allowed.
         */
        export const hasMaximumPrecisionOf = (
            maximumPrecision: number,
        ): ((val: string) => string) => {
            const numberToMathBy = Math.pow(10, maximumPrecision);
            const error = `Your input may only have ${maximumPrecision} decimal place(s).`;
            return (val: string) => {
                try {
                    const input = val.toString();
                    return (Math.round(Number(val) * numberToMathBy) /
                        numberToMathBy).toString() === input
                        ? ''
                        : error;
                } catch (e) {
                    return error;
                }
            };
        };

        /**
         * Returns an error if input contains numeric characters
         */
        export const nonNumeric = (val: string): string =>
            !val || !val.trim() || /^([^0-9]*)$/.exec(val)
                ? ''
                : 'Numbers are not allowed.';
    }

    export namespace Formatters {
        export const formatPhoneNumber = (val: string): string => {
            try {
                return phoneUtil.format(
                    phoneUtil.parse(val, 'US'),
                    PhoneNumberFormat.NATIONAL,
                );
            } catch (e) {
                return val;
            }
        };

        export const capitalizeFirstLetterOfEachWord = (
            val: string,
        ): string => {
            try {
                return val
                    .split(' ')
                    .map(word => word[0].toUpperCase() + word.slice(1))
                    .join(' ');
            } catch (e) {
                return val;
            }
        };

        /**
         * FACTORY: Returns an error if input is more precise than allowed.
         */
        export const hasMaximumPrecisionOf = (
            maximumPrecision: number,
        ): ((val: string) => string) => {
            const numberToMathBy = Math.pow(10, maximumPrecision);
            return (val: string) => {
                try {
                    if (val[val.length - 1] === '.' || isNaN(Number(val))) {
                        return val;
                    }
                    return (Math.round(Number(val) * numberToMathBy) /
                        numberToMathBy).toString();
                } catch (e) {
                    return val;
                }
            };
        };
    }
}
