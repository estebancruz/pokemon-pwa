# BR-Redux

This is a set of a few helper functions intended to assist in the templetation / consistency of of redux implementations.

This library provides:

* Action-creator creators
* FetchStatus enum
* Creatable/Patchable generic types