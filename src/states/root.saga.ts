import { pokemonSaga } from './pokemon/pokemon.saga';

export default function* rootSaga() {
    yield [pokemonSaga()];
}
