import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import { pokemonReducer as pokemon } from './pokemon';
import { viewReducer as view } from './view';

export default combineReducers({
    // tslint:disable:object-literal-sort-keys
    routing: routing as any, // smelly, but fixes error
    // add more reducers here
    view,
    pokemon,
});
