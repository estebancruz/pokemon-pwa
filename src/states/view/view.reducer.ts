import { ActionType, ViewAction } from './view.actions';

interface IViewState {
    readonly loadingRequests: ReadonlyArray<symbol>;
}

const DEFAULT_STATE: IViewState = {
    loadingRequests: [],
};

const { REQUEST_LOADING_INDICATOR, DISMISS_LOADING_INDICATOR } = ActionType;

const viewReducer = (
    state: IViewState = DEFAULT_STATE,
    action: ViewAction | { readonly type: '' },
): IViewState => {
    switch (action.type) {
        case REQUEST_LOADING_INDICATOR:
            return {
                ...state,
                loadingRequests: [...state.loadingRequests, action.payload],
            };
        case DISMISS_LOADING_INDICATOR:
            const symbolIdx = state.loadingRequests.indexOf(action.payload);
            return {
                ...state,
                loadingRequests: [
                    ...state.loadingRequests.slice(0, symbolIdx),
                    ...state.loadingRequests.slice(symbolIdx + 1),
                ],
            };
        default:
            return state;
    }
};

export { DEFAULT_STATE, IViewState, viewReducer };
