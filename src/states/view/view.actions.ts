import { createAction, IAction } from 'lib/br-redux';

// ------------------------------------
// ActionType
// Used as the "type" value in an action object.
// ------------------------------------

enum ActionType {
    REQUEST_LOADING_INDICATOR = 'VIEW/REQUEST_LOADING_INDICATOR',
    DISMISS_LOADING_INDICATOR = 'VIEW/DISMISS_LOADING_INDICATOR',
}

// ------------------------------------
// ActionTypes
// Used for type discrimination in reducers/middleware
// ------------------------------------

type RequestLoadingIndicatorAction = IAction<
    ActionType.REQUEST_LOADING_INDICATOR,
    symbol
>;
type DismissLoadingIndicatorAction = IAction<
    ActionType.DISMISS_LOADING_INDICATOR,
    symbol
>;

// ------------------------------------
// Action Union Type
// ------------------------------------

type ViewAction = RequestLoadingIndicatorAction | DismissLoadingIndicatorAction;

// ------------------------------------
// Action Creators
// i.e. the stuff that actually makes it to the compiled Javascript
// ------------------------------------

const requestLoadingIndicator = createAction(
    ActionType.REQUEST_LOADING_INDICATOR,
    (key: symbol) => key,
);
const dismissLoadingIndicator = createAction(
    ActionType.DISMISS_LOADING_INDICATOR,
    (key: symbol) => key,
);

export {
    ActionType,
    dismissLoadingIndicator,
    DismissLoadingIndicatorAction,
    requestLoadingIndicator,
    RequestLoadingIndicatorAction,
    ViewAction,
};
