import * as actions from './view.actions';

describe('View Actions', () => {
    it('requestLoadingIndicator', () => {
        const key = Symbol('SomeAsyncFunction');
        const { payload } = actions.requestLoadingIndicator(key);
        expect(payload).toEqual(key);
    });
});
