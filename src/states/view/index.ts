import { viewReducer } from 'states/view/view.reducer';
import * as ViewActions from './view.actions';

export { ViewActions, viewReducer };
