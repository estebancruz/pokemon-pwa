import {
    dismissLoadingIndicator,
    requestLoadingIndicator,
} from './view.actions';
import { DEFAULT_STATE, viewReducer } from './view.reducer';

describe('viewReducer', () => {
    test('Has default state', () => {
        expect(viewReducer(undefined, { type: '' })).toEqual(DEFAULT_STATE);
    });

    test('Does not modify existing state', () => {
        const request = Symbol('Test_SYMBOL');
        Object.freeze(DEFAULT_STATE);
        viewReducer(DEFAULT_STATE, requestLoadingIndicator(request));
        viewReducer(DEFAULT_STATE, dismissLoadingIndicator(request));
    });

    test('Can add and remove loading requests', () => {
        const request1 = Symbol('request_1');
        const request2 = Symbol('request_2');

        // Reducer receives action we don't care about
        // tslint:disable-next-line:no-let
        let state = viewReducer(undefined, { type: '' });
        expect(state).toEqual(DEFAULT_STATE);
        expect(state.loadingRequests.length).toEqual(0);

        // Reducer receives first loading request
        state = viewReducer(state, requestLoadingIndicator(request1));
        expect(state.loadingRequests.length).toEqual(1);

        // Reducer receives second loading request
        state = viewReducer(state, requestLoadingIndicator(request2));
        expect(state.loadingRequests.length).toEqual(2);

        // Second request removed
        state = viewReducer(state, dismissLoadingIndicator(request2));
        expect(state.loadingRequests.length).toEqual(1);
        expect(state.loadingRequests.indexOf(request2)).toEqual(-1);

        // Second request removed again (no change)
        state = viewReducer(state, dismissLoadingIndicator(request2));
        expect(state.loadingRequests.length).toEqual(1);
        expect(state.loadingRequests.indexOf(request1)).toEqual(0);

        // Re-add request 2 and remove request 1
        state = viewReducer(state, requestLoadingIndicator(request2));
        state = viewReducer(state, dismissLoadingIndicator(request1));
        expect(state.loadingRequests.length).toEqual(1);
        expect(state.loadingRequests.indexOf(request2)).toEqual(0);
    });
});
