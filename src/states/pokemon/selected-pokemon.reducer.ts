import { FetchStatus } from 'lib/br-redux';
import { ActionType, IPokemonAction } from './pokemon.actions';

interface IPokemonState {
    readonly fetchStatus: FetchStatus;
    readonly pokemon: {};
}

const DEFAULT_STATE: IPokemonState = {
    fetchStatus: FetchStatus.NOT_FETCHED,
    pokemon: {},
};

const { LOAD_SELECTED_POKEMON } = ActionType;

const pokemonReducer = (
    state: IPokemonState = DEFAULT_STATE,
    action: IPokemonAction | { readonly type: {} },
): IPokemonState => {
    switch (action.type) {
        case LOAD_SELECTED_POKEMON:
            return {
                ...state,
                ...{
                    fetchStatus: FetchStatus.SUCCESS,
                    pokemon: require('../../mocks/selected-pokemon.json'),
                },
            };
        default:
            return state;
    }
};

export { DEFAULT_STATE, IPokemonState, pokemonReducer };
