import { createAction, IAction } from 'lib/br-redux';
import { IListablePokemon, ISelectedPokemon } from 'models';

// ------------------------------------
// ActionType
// Used as the "type" value in an action object.
// ------------------------------------

enum ActionType {
    SELECT_GENERATION = 'POKEMON/SELECT_GENERATION',
    LOAD_POKEMON_LIST = 'POKEMON/LOAD_POKEMON_LIST',
    SELECT_POKEMON = 'POKEMON/SELECT_POKEMON',
    LOAD_SELECTED_POKEMON = 'POKEMON/LOAD_SELECTED_POKEMON',

    REPORT_ERROR = 'POKEMON/REPORT_ERROR',
}

// ------------------------------------
// ActionTypes
// Used for type discrimination in reducers/middleware
// ------------------------------------

type ISelectGenerationAction = IAction<ActionType.SELECT_GENERATION, number>;

type ILoadPokemonListAction = IAction<
    ActionType.LOAD_POKEMON_LIST,
    ReadonlyArray<IListablePokemon>
>;

type ISelectPokemonAction = IAction<ActionType.SELECT_POKEMON, number>;

type ILoadSelectedPokemonAction = IAction<
    ActionType.LOAD_SELECTED_POKEMON,
    ISelectedPokemon
>;

type IReportErrorAction = IAction<ActionType.REPORT_ERROR, string>;

// ------------------------------------
// Action Union Type
// ------------------------------------

type IPokemonAction =
    | ISelectGenerationAction
    | ILoadPokemonListAction
    | ISelectPokemonAction
    | ILoadPokemonListAction
    | IReportErrorAction;

// ------------------------------------
// Action Creators
// i.e. the stuff that actually makes it to the compiled Javascript
// ------------------------------------

const selectGeneration = createAction(
    ActionType.SELECT_GENERATION,
    (id: number) => id,
);
const loadPokemonList = createAction(
    ActionType.LOAD_POKEMON_LIST,
    (pokemon: ReadonlyArray<IListablePokemon>) => pokemon,
);
const selectPokemon = createAction(
    ActionType.SELECT_POKEMON,
    (id: number) => id,
);
const loadSelectedPokemon = createAction(
    ActionType.LOAD_SELECTED_POKEMON,
    (pokemon: ISelectedPokemon) => pokemon,
);
const reportError = createAction(
    ActionType.REPORT_ERROR,
    (error: string) => error,
);

export {
    ActionType,
    ISelectGenerationAction,
    ILoadPokemonListAction,
    ISelectPokemonAction,
    ILoadSelectedPokemonAction,
    IReportErrorAction,
    selectGeneration,
    loadPokemonList,
    selectPokemon,
    loadSelectedPokemon,
    reportError,
    IPokemonAction,
};
