import { FetchStatus } from 'lib/br-redux';
import { IListablePokemon } from 'models';
import { ActionType, IPokemonAction } from './pokemon.actions';

interface IPokemonState {
    readonly fetchStatus: FetchStatus;
    readonly list: ReadonlyArray<IListablePokemon>;
    readonly selectedGeneration: number;
}

const DEFAULT_STATE: IPokemonState = {
    fetchStatus: FetchStatus.NOT_FETCHED,
    list: [],
    selectedGeneration: 0,
};

const { SELECT_GENERATION, LOAD_POKEMON_LIST } = ActionType;

const pokemonReducer = (
    state: IPokemonState = DEFAULT_STATE,
    action: IPokemonAction | { readonly type: '' },
): IPokemonState => {
    switch (action.type) {
        case SELECT_GENERATION:
            return {
                ...state,
                ...{
                    fetchStatus: FetchStatus.FETCHING,
                    selectedGeneration: action.payload,
                },
            };
        case LOAD_POKEMON_LIST:
            return {
                ...state,
                ...{
                    fetchStatus: FetchStatus.SUCCESS,
                    list: action.payload,
                },
            };
        default:
            return state;
    }
};

export { DEFAULT_STATE, IPokemonState, pokemonReducer };
