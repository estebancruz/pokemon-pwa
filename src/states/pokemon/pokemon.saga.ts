import { IGenerationResponse } from 'models';
import { call, put, takeEvery } from 'redux-saga/effects';
import { PokemonService } from 'services';
import {
    dismissLoadingIndicator,
    requestLoadingIndicator,
} from 'states/view/view.actions';
import {
    ActionType,
    ISelectGenerationAction,
    ISelectPokemonAction,
    loadPokemonList,
    reportError,
} from './pokemon.actions';

// tslint:disable:no-console
function* fetchGeneration(action: ISelectGenerationAction) {
    const loadingKey = Symbol('fetchGeneration');
    try {
        yield put(requestLoadingIndicator(loadingKey));
        const response: IGenerationResponse = yield call(
            PokemonService.fetchGeneration,
            action.payload,
        );
        yield put(loadPokemonList(response.pokemon_species));
    } catch (err) {
        yield put(reportError((err as Error).message));
    } finally {
        yield put(dismissLoadingIndicator(loadingKey));
    }
}

function* fetchPokemon(action: ISelectPokemonAction) {
    const loadingKey = Symbol('fetchPokemon');
    try {
        yield put(requestLoadingIndicator(loadingKey));
        const response = yield call(
            PokemonService.fetchPokemon,
            action.payload,
        );
        console.log(response);
    } catch (err) {
        yield put(reportError((err as Error).message));
    } finally {
        yield put(dismissLoadingIndicator(loadingKey));
    }
}

function* pokemonSaga() {
    yield [
        takeEvery(ActionType.SELECT_GENERATION, fetchGeneration),
        takeEvery(ActionType.SELECT_POKEMON, fetchPokemon),
    ];
}

export { pokemonSaga };
