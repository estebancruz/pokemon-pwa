import { expectSaga, testSaga } from 'redux-saga-test-plan';
import rootSaga from './root.saga';

describe('root saga', () => {
    it('runs the root saga', () => {
        return expectSaga(rootSaga).silentRun();
    });

    it('manually calls the root saga', () => {
        testSaga(rootSaga).next().next().returns();
    });
});
