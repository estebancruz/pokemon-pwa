import configureStore from './store';

describe('configureStore', () => {
    // tslint:disable-next-line:no-let
    let store;

    beforeAll(() => {
        store = configureStore();
    });

    describe('configureStore', () => {
        it('should contain an object', () => {
            expect(typeof store).toBe('object');
        });
    });
});
