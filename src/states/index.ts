import RootReducer from './root.reducer';
import configureStore from './store';

export { configureStore, RootReducer };
