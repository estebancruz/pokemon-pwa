import { applyMiddleware, createStore, Store } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import IApplicationState from 'models/application-state';
import rootReducer from './root.reducer';
import rootSaga from './root.saga';

const sagaMiddleware = createSagaMiddleware();

/**
 * Configures the starting state for the application.
 * @param preloadedState for the state startup.
 * @returns {Store<S>} initial state.
 */
const configureStore = (
    preloadedState: Partial<IApplicationState> = {},
): Store<Partial<IApplicationState>> => {
    const enhancers: ReadonlyArray<any> = [sagaMiddleware, logger];

    const middleware = applyMiddleware(...enhancers);
    const store = createStore(rootReducer, preloadedState, middleware);

    sagaMiddleware.run(rootSaga);

    if ((module as any).hot) {
        // Enable Webpack hot module replacement for reducers
        (module as any).hot.accept('./root.reducer', () => {
            const nextRootReducer = require('./root.reducer').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
};

export default configureStore;
