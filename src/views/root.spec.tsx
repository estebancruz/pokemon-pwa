import { shallow } from 'enzyme';
import * as React from 'react';
import configureStore from 'states/store';
import Root from './root';

describe('Root View', () => {
    const store = configureStore();
    test('Renders correctly', () => {
        const root = shallow(<Root store={store} />);
        expect(root.length).toBe(1);
    });
});
