import { Grid, Typography as T } from 'components';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const { Col, Row } = Grid;
const styles = require('./detail.less');

const Detail = () =>
    <div>
        <Row>
            <Col xs={12}>
                <T.Header>Detail</T.Header>
            </Col>
        </Row>
    </div>;

export default CSSModules(Detail, styles, { allowMultiple: true });
