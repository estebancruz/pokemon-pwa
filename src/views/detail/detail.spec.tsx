import { shallow } from 'enzyme';
import * as React from 'react';
import Detail from './detail';

describe('Home View', () => {
    test('Renders correctly', () => {
        const home = shallow(<Detail />);
        expect(home.type()).toEqual('div');
    });
});
