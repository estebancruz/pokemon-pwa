import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import App from './app';

/**
 * Imported root css file.
 */
const styles = require('./root.less');

@CSSModules(styles)
class Root extends React.Component<IProps, void> {
    /**
     * Renders the view.
     * @returns {any} the rendered component.
     */
    render(): JSX.Element {
        return (
            <Provider store={this.props.store}>
                <App />
            </Provider>
        );
    }
}

/**
 * Properties available on the <code>props</code> property.
 */
interface IProps {
    readonly store: Store<any>;
}

export default Root;
