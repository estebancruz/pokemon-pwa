import { shallow } from 'enzyme';
import * as React from 'react';
import List from './list';

describe('Home View', () => {
    test('Renders correctly', () => {
        const home = shallow(<List />);
        expect(home.type()).toEqual('div');
    });
});
