import { Grid, Typography as T } from 'components';
import { Button } from 'components/buttons';
import { IListablePokemon } from 'models';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { connect } from 'react-redux';
import {
    selectGeneration,
    selectPokemon,
} from 'states/pokemon/pokemon.actions';

const { Col, Row } = Grid;
const styles = require('./list.less');

interface Props {
    readonly pokemon: ReadonlyArray<IListablePokemon>;
    selectGeneration(): void;
    selectPokemon(): void;
}

@CSSModules(styles, { allowMultiple: true })
class List extends React.Component<Props, void> {
    render() {
        const { selectGeneration, selectPokemon } = this.props;
        return (
            <div>
                <Row>
                    <Col xs={12}>
                        <T.Header>List</T.Header>
                        <ul>
                            {this.props.pokemon.map(p =>
                                <li key={p.name}>
                                    {p.name}
                                </li>,
                            )}
                        </ul>
                        <Button onClick={selectGeneration} primary>
                            Select Gen 1
                        </Button>
                        <Button danger onClick={selectPokemon}>
                            Select Squirtle
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    pokemon: state.pokemon.list,
});
const mapDispatchToProps = dispatch => ({
    selectGeneration: () => dispatch(selectGeneration(1)),
    selectPokemon: () => dispatch(selectPokemon(7)),
});

export default connect(mapStateToProps, mapDispatchToProps)(List);
