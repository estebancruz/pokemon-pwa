import { Header } from 'components/typography';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./four-oh-four.less');

const FourOhFour = () =>
    <article>
        <div styleName="fourOhFourContainer">
            <Header>404</Header>
        </div>
    </article>;

export default CSSModules(FourOhFour, styles, {});
