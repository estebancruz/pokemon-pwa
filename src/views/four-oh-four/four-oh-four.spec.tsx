import { shallow } from 'enzyme';
import * as React from 'react';
import FourOhFour from './four-oh-four';

describe('404', () => {
    test('Renders correctly', () => {
        const fourOhFour = shallow(<FourOhFour />);
        expect(fourOhFour.type()).toEqual('article');
    });
});
