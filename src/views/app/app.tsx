import { FullScreenLoadingIndicator, Navigation } from 'components';
import { Container } from 'components/grid';
import { IApplicationState } from 'models';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Detail from 'views/detail';
import FourOhFour from 'views/four-oh-four';
import List from 'views/list';

/**
 * Imported styles.
 */
const styles = require('./app.less');

@CSSModules(styles)
class App extends React.Component<IProps, void> {
    /**
     * Renders the view.
     * @returns {any} the rendered component.
     */
    render(): JSX.Element {
        return (
            <Router>
                <div styleName="app">
                    <Navigation />
                    <Container>
                        <main styleName="contentContainer">
                            <Switch>
                                <Route exact path="/" component={List} />
                                <Route exact path="/:id" component={Detail} />
                                <Route component={FourOhFour} />
                            </Switch>
                        </main>
                    </Container>
                    <FullScreenLoadingIndicator
                        visible={this.props.isLoading}
                    />
                </div>
            </Router>
        );
    }
}

/**
 * Properties available on the <code>props</code> property.
 */
interface IProps {
    readonly isLoading: boolean;
}

/**
 * Maps <code>state</code> properties to <code>props</code>.
 */
const mapStateToProps = (state: IApplicationState) => ({
    isLoading: !!(
        state.view.loadingRequests && state.view.loadingRequests.length
    ),
});

export default connect(mapStateToProps)(App);
