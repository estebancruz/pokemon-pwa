interface IListablePokemon {
    readonly name: string;
    readonly url: string;
}

class ListablePokemon {
    private obj: IListablePokemon;

    constructor(obj: IListablePokemon) {
        this.obj = obj;
    }
}

export { IListablePokemon, ListablePokemon };
