interface ISelectedPokemon {
    readonly name: string;
    readonly weight: number;
    readonly types: {
        readonly type: {
            readonly name: string;
        };
    };
}

class SelectedPokemon {
    private obj: ISelectedPokemon;

    constructor(obj: ISelectedPokemon) {
        this.obj = obj;
    }

    get types(): string {
        return this.obj.types.type.name;
    }
}

export { ISelectedPokemon, SelectedPokemon };
