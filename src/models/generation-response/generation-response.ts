import { IListablePokemon } from 'models/listable-pokemon';

interface IGenerationResponse {
    readonly pokemon_species: ReadonlyArray<IListablePokemon>;
}

export { IGenerationResponse };
