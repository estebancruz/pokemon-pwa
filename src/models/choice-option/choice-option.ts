interface ChoiceOption {
    readonly value: string;
    readonly label: string;
    readonly selected?: boolean;
}

export default ChoiceOption;
