import ChoiceOption from './choice-option';

const options: ChoiceOption = { label: 'No', value: 'no' };

describe('Choice Option Model', () => {
    it('is defined', () => {
        expect(options).toBeDefined();
    });

    it('is an object', () => {
        expect(typeof options).toBe('object');
    });

    it('has a label of type string', () => {
        expect(typeof options.label).toBe('string');
    });

    it('has a value of type string', () => {
        expect(typeof options.value).toBe('string');
    });

    it('should contain a label', () => {
        expect(options.label).toEqual('No');
    });

    it('should contain a value', () => {
        expect(options.value).toEqual('no');
    });
});
