import { IViewState } from 'states/view/view.reducer';

/**
 * Base application state. All models and model sets
 * are added to this object.
 */
interface IApplicationState {
    readonly view: IViewState;
}

export default IApplicationState;
