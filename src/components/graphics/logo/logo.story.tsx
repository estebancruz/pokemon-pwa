import { storiesOf } from '@kadira/storybook';
import * as React from 'react';
import Logo from './logo';

storiesOf('Graphics', module).add('Logo', () =>
    <div>
        <Logo />
    </div>,
);
