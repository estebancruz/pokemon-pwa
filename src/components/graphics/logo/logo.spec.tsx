import { shallow } from 'enzyme';
import * as React from 'react';
import Logo from './logo';

describe('Logo', () => {
    it('should render an <svg> tag', () => {
        const renderedComponent = shallow(<Logo />);
        expect(renderedComponent.type()).toEqual('svg');
    });
});
