import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./glyphicon.less');
import Icons from './glyphicon.constants';
import Props from './glyphicon.props';

const Glyphicon = (props: Props) =>
    <span
        styleName={[
            'glyphicon',
            props.spin ? 'spin' : '',
            ...Object.keys(props).map(
                p => (p && props[p] === true ? Icons[p] : ''),
            ),
        ].join(' ')}
        aria-hidden="true"
    />;
export default CSSModules(Glyphicon, styles, { allowMultiple: true });
