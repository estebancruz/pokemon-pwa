import { shallow } from 'enzyme';
import * as React from 'react';
import * as renderer from 'react-test-renderer';
import Glyphicon from './glyphicon';
import Icons from './glyphicon.constants';

describe('Glyphicon', () => {
    test('test snapshot', () => {
        const tree = renderer.create(<Glyphicon />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('should render a <span> tag', () => {
        const renderedComponent = shallow(<Glyphicon />);
        expect(renderedComponent.type()).toEqual('span');
    });

    it('should accept icon types', () => {
        const renderedComponent = shallow(<Glyphicon magnet />);
        expect(renderedComponent.hasClass(Icons.magnet)).toEqual(true);
    });
});
