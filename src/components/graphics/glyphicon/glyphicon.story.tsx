import { storiesOf } from '@kadira/storybook';
import * as React from 'react';
import Glyphicon from './glyphicon';

storiesOf('Graphics', module).add('Bootstrap Glyphicon', () =>
    <div>
        <p>
            <Glyphicon asterisk />
            <Glyphicon plus />
            <Glyphicon euro />
            <Glyphicon eur />
            <Glyphicon minus />
            <Glyphicon cloud />
            <Glyphicon envelope />
            <Glyphicon pencil />
            <Glyphicon glass />
            <Glyphicon music />
            <Glyphicon search />
            <Glyphicon heart />
            <Glyphicon star />
            <Glyphicon starEmpty />
            <Glyphicon user />
        </p>
        <p>For more options, visit:</p>
        <a href="http://getbootstrap.com/components/#glyphicons">Bootstrap</a>
    </div>,
);
