import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./text-area.less');

interface Props extends React.HTMLProps<HTMLTextAreaElement> {
    readonly id: string;
}

const TextArea = ({ children, type, styleName, ...props }: Props) =>
    <textarea styleName="text-area" type={type || 'text'} {...props}>
        {children}
    </textarea>;

export default CSSModules(TextArea, styles, { allowMultiple: true });
