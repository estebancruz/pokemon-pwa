import { shallow } from 'enzyme';
import * as React from 'react';
import TextArea from './text-area';

describe('TextArea Input ', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render an textarea type', () => {
        const renderedComponent = shallow(<TextArea id={mock} />);
        expect(renderedComponent.type()).toEqual('textarea');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(<TextArea id={mock} />);
        expect(renderedComponent.prop('className')).toEqual('text-area');
    });

    it('should have a id prop', () => {
        const renderedComponent = shallow(<TextArea id={mock} />);
        expect(renderedComponent.prop('id')).toEqual('test');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<TextArea id={mock} />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <TextArea id={mock}>
                {children}
            </TextArea>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
