import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import TextArea from './text-area';

storiesOf('Input', module).add('Text Input', () =>
    <TextArea id="storybook-text-input">Placeholder...</TextArea>,
);
