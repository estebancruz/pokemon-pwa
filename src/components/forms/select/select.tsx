import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { ChoiceOption } from 'models';

const styles = require('./select.less');

interface Props extends React.HTMLProps<HTMLSelectElement> {
    readonly options: ReadonlyArray<ChoiceOption>;
    readonly error?: boolean;
}

const Select = ({ options, error, styleName, className, ...props }: Props) => {
    const selectedOption = options.find(o => !!o.selected);
    const value = selectedOption ? selectedOption.value : '';
    return (
        <div
            styleName={['select', error ? 'error' : '', styleName].join(' ')}
            className={className}
        >
            <select value={value} {...props}>
                {options.map(optionalize)}
            </select>
            <span styleName="arrow">^</span>
        </div>
    );
};

const optionalize = (opt: ChoiceOption, idx: number) =>
    <option key={`${opt.value}_${idx}`} value={opt.value}>
        {opt.label}
    </option>;

export default CSSModules(Select, styles, { allowMultiple: true });
