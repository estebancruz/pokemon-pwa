import { storiesOf } from '@kadira/storybook';
import { ChoiceOption } from 'models';
import * as React from 'react';
import Select from './select';

const mock = 'test';
const options: ReadonlyArray<ChoiceOption> = [
    { label: 'No', value: 'no' },
    { label: 'Yes', value: 'yes' },
];

storiesOf('Input', module).add('Select', () =>
    <div>
        <Select id={mock} name={mock} options={options} />
    </div>,
);
