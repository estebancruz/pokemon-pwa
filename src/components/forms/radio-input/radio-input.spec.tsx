import { shallow } from 'enzyme';
import * as React from 'react';
import RadioButton from './radio-input';

describe('RadioButton', () => {
    const mock = 'test';
    it('should render an outer div', () => {
        const renderedComponent = shallow(
            <RadioButton id={mock} name={mock} value={mock} />,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <RadioButton id={mock} name={mock} value={mock} />,
        );
        expect(renderedComponent.prop('className')).toBeDefined();
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <RadioButton id={mock} name={mock} value={mock} />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
