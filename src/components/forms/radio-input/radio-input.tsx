import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./radio-input.less');

interface Props extends React.HTMLProps<HTMLInputElement> {
    readonly id: string;
    readonly name: string;
    readonly value: string;
}

const RadioButton = ({ styleName, type, ...props }: Props) =>
    <div styleName={['radio-input', styleName].join(' ')}>
        <input styleName="input" type="radio" {...props} />
        <span styleName="outer-circle" />
        <span styleName="inner-circle" />
    </div>;

export default CSSModules(RadioButton, styles, { allowMultiple: true });
