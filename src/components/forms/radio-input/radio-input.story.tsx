import { storiesOf } from '@kadira/storybook';
import * as React from 'react';
import RadioInput from './radio-input';

const mock = 'test';

storiesOf('Input', module).add('Radio', () =>
    <div>
        <RadioInput id={mock} name={mock} value={mock} />
    </div>,
);
