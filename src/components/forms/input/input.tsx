import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./input.less');

interface Props extends React.HTMLProps<HTMLInputElement> {
    readonly id: string;
    readonly styleName?: string;
}

const TextInput = ({ styleName, type, ...props }: Props) =>
    <input
        styleName={['form-control', styleName || ''].join(' ')}
        type={type || 'text'}
        {...props}
    />;

export default CSSModules(TextInput, styles, { allowMultiple: true });
