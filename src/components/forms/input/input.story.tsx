import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import TextInput from './input';

storiesOf('Input', module).add('Text Input', () =>
    <TextInput id="storybook-text-input" placeholder="Placeholder Text" />,
);
