import { shallow } from 'enzyme';
import * as React from 'react';
import TextInput from './input';

describe('Input', () => {
    const mock = 'test';
    it('should render an input type', () => {
        const renderedComponent = shallow(<TextInput id={mock} />);
        expect(renderedComponent.type()).toEqual('input');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(<TextInput id={mock} />);
        expect(renderedComponent.prop('className')).toEqual('form-control');
    });

    it('should have a id prop', () => {
        const renderedComponent = shallow(<TextInput id={mock} />);
        expect(renderedComponent.prop('id')).toEqual('test');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<TextInput id={mock} />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
