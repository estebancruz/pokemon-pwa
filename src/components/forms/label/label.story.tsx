import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import Label from './label';

storiesOf('Input', module).add('Label', () =>
    <Label id="labelAtom" htmlFor="inputAtom" className="asc_label">
        Label Example
    </Label>,
);
