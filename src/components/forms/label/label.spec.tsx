import { shallow } from 'enzyme';
import * as React from 'react';
import Label from './label';

describe('Label', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render an <Label> type', () => {
        const renderedComponent = shallow(
            <Label id={mock} name={mock} htmlFor={mock} />,
        );
        expect(renderedComponent.type()).toEqual('label');
    });

    it('should have a id prop', () => {
        const renderedComponent = shallow(
            <Label id={mock} name={mock} htmlFor={mock} />,
        );
        expect(renderedComponent.prop('id')).toBeDefined();
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <Label id={mock} name={mock} htmlFor={mock} />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Label id={mock} name={mock} htmlFor={mock}>
                {children}
            </Label>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
