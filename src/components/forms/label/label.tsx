import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./label.less');

interface Props extends React.HTMLProps<HTMLLabelElement> {
    readonly id: string;
    readonly htmlFor: string;
}

const Label = ({ htmlFor, children, ...props }: Props) =>
    <label htmlFor={htmlFor} styleName="label" {...props}>
        {children}
    </label>;

export default CSSModules(Label, styles, { allowMultiple: true });
