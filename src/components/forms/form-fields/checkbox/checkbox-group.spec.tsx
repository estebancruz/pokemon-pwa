import { shallow } from 'enzyme';
import { ChoiceOption } from 'models';
import * as React from 'react';
import CheckboxGroup from './checkbox-group';

const options: ReadonlyArray<ChoiceOption> = [
    { label: 'No', value: 'no' },
    { label: 'Yes', value: 'yes' },
];

describe('CheckboxGroup', () => {
    const mock = 'test';
    it('should render', () => {
        const renderedComponent = shallow(
            <CheckboxGroup
                onChange={() => 'woop'}
                id={mock}
                label={mock}
                options={options}
            />,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <CheckboxGroup
                onChange={() => 'woop'}
                id={mock}
                label={mock}
                options={options}
            />,
        );
        expect(renderedComponent.prop('className')).toBeDefined();
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <CheckboxGroup
                onChange={() => 'woop'}
                id={mock}
                label={mock}
                options={options}
            />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
