import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { ChoiceOption } from 'models';
import CheckboxOption from './checkbox-option';

const styles = require('./checkbox-group.css');

interface Props {
    readonly name?: string;
    readonly id: string;
    readonly label: string;
    readonly onChange: React.FormEventHandler<HTMLInputElement>;
    readonly options: ReadonlyArray<ChoiceOption>;
    readonly styleName?: string;
    readonly error?: string;
    readonly className?: string;
}

const CheckboxGroup = ({
    className,
    name,
    error,
    id,
    label,
    options,
    onChange,
    styleName,
}: Props) =>
    <div className={className} styleName={['radio-group', styleName].join(' ')}>
        <span styleName="group-label">
            {label}
        </span>
        {options.map((o, idx) =>
            <CheckboxOption
                styleName="group-option"
                key={`${o.value}_${idx}`}
                label={o.label}
                value={o.value}
                checked={o.selected}
                name={name || id}
                id={`${id}_${o.value}_${idx}`}
                onChange={onChange}
            />,
        )}
        <span styleName="error-message">
            {error}
        </span>
    </div>;

export default CSSModules(CheckboxGroup, styles, { allowMultiple: true });
