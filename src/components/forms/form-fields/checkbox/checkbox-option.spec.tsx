import { shallow } from 'enzyme';
import * as React from 'react';
import CheckboxOption from './checkbox-option';

describe('CheckboxOption', () => {
    const mock = 'test';
    it('should render', () => {
        const renderedComponent = shallow(
            <CheckboxOption name={mock} value={mock} id={mock} label={mock} />,
        );
        expect(renderedComponent.text()).toEqual('<Label />');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <CheckboxOption name={mock} value={mock} id={mock} label={mock} />,
        );
        expect(renderedComponent.prop('className')).toBeDefined();
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <CheckboxOption name={mock} value={mock} id={mock} label={mock} />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
