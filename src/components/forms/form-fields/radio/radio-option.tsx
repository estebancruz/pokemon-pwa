import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { Label, RadioInput } from 'components/forms';

const styles = require('./radio-option.css');

interface Props extends React.HTMLProps<HTMLInputElement> {
    readonly id: string;
    readonly name: string;
    readonly value: string;
    readonly label: string;
}

const RadioOption = ({ styleName, className, id, label, ...props }: Props) =>
    <Label
        className={className}
        styleName={['radio-option', styleName].join(' ')}
        id={`${id}_label`}
        htmlFor={id}
    >
        <RadioInput id={id} {...props} />
        <span styleName="label">
            {label}
        </span>
    </Label>;

export default CSSModules(RadioOption, styles, { allowMultiple: true });
