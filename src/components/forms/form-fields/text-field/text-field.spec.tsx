import { shallow } from 'enzyme';
import * as React from 'react';
import TextField from './textfield';

describe('TextField', () => {
    const mock = 'test';
    it('should render an input type', () => {
        const renderedComponent = shallow(
            <TextField label={mock} id="text-field" />,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <TextField label={mock} id="text-field" />,
        );
        expect(renderedComponent.prop('className')).toEqual('text-field');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <TextField label={mock} id="text-field" />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
