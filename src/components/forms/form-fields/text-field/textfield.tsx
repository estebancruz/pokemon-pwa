import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { Input, Label } from 'components/forms';

import { optionalize } from 'components/utility';

const styles = require('./textfield.css');

interface Props extends React.HTMLProps<HTMLInputElement> {
    readonly id: string;
    readonly label: string;
    readonly error?: string;
}

const TextField = ({
    styleName,
    className,
    label,
    error,
    id,
    ...props,
}: Props) =>
    <div
        styleName={[
            'text-field',
            styleName,
            ...optionalize({ error: !!(error && error.trim()) }),
        ].join(' ')}
        className={className}
    >
        <Label id={`${id}_label`} htmlFor={id}>
            {label}
        </Label>
        <Input id={id} {...props} />
        <span styleName="error-message">
            {error}
        </span>
    </div>;

export default CSSModules(TextField, styles, { allowMultiple: true });
