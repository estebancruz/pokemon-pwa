import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import TextField from './textfield';

storiesOf('Molecules', module).add('Text Input', () =>
    <TextField id="text_input_example" label="Text Input Example" />,
);
