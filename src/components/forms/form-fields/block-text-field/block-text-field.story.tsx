import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import BlockTextField from './block-text-field';

storiesOf('Molecules', module).add('Block Text Input', () =>
    <BlockTextField
        id="text_input_example"
        label="Block Text Example"
        placeholder="This is some placeholder text."
    />,
);
