import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { Label, TextArea } from 'components/forms';

import { optionalize } from 'components/utility';

const styles = require('./block-text-field.css');

interface Props extends React.HTMLProps<HTMLTextAreaElement> {
    readonly id: string;
    readonly label: string;
    readonly error?: string;
}

const BlockTextField = ({
    name,
    error,
    styleName,
    className,
    label,
    id,
    placeholder,
    ...props,
}: Props) =>
    <div
        styleName={[
            'block-text-field',
            styleName,
            ...optionalize({ error: !!(error && error.trim()) }),
        ].join(' ')}
        className={className}
    >
        <Label id={`${id}_label`} htmlFor={id}>
            {label}
        </Label>
        <TextArea id={id} name={name || id} {...props} />
        <span styleName="error-message">
            {error}
        </span>
    </div>;

export default CSSModules(BlockTextField, styles, { allowMultiple: true });
