import { shallow } from 'enzyme';
import * as React from 'react';
import BlockTextField from './block-text-field';

describe('BlockTextField', () => {
    const mock = 'test';
    it('should render an input type', () => {
        const renderedComponent = shallow(
            <BlockTextField id={mock} label={mock} />,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <BlockTextField id={mock} label={mock} />,
        );
        expect(renderedComponent.prop('className')).toEqual('block-text-field');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <BlockTextField id={mock} label={mock} />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
