import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { Label, Select } from 'components/forms';
import { ChoiceOption } from 'models';
const styles = require('./select-field.css');

interface Props extends React.HTMLProps<HTMLSelectElement> {
    readonly id: string;
    readonly label: string;
    readonly error?: string;
    readonly options: ReadonlyArray<ChoiceOption>;
}

const SelectFieldComponent = ({
    options,
    styleName,
    className,
    label,
    error,
    id,
    ...props,
}: Props) =>
    <div
        styleName={['select-field', styleName].join(' ')}
        className={className}
    >
        <Label id={`${id}_label`} htmlFor={id}>
            {label}
        </Label>
        <Select
            {...props}
            id={id}
            options={options}
            error={!!(error && error.trim())}
            styleName="select"
        />
        <span styleName="error-message">
            {error}
        </span>
    </div>;

export const SelectField = CSSModules(SelectFieldComponent, styles, {
    allowMultiple: true,
});
