import { shallow } from 'enzyme';
import { ChoiceOption } from 'models';
import * as React from 'react';
import { SelectField } from './select-field';

const options: ReadonlyArray<ChoiceOption> = [
    { label: 'No', value: 'no' },
    { label: 'Yes', value: 'yes' },
];

describe('SelectField', () => {
    const mock = 'test';
    it('should render', () => {
        const renderedComponent = shallow(
            <SelectField id={mock} label={mock} options={options} />,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <SelectField id={mock} label={mock} options={options} />,
        );
        expect(renderedComponent.prop('className')).toBeDefined();
    });

    it('should have the selectfield className', () => {
        const renderedComponent = shallow(
            <SelectField id={mock} label={mock} options={options} />,
        );
        expect(renderedComponent.prop('className')).toEqual('select-field');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <SelectField id={mock} label={mock} options={options} />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
