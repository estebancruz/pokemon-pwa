import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./checkbox-input.less');

interface Props extends React.HTMLProps<HTMLInputElement> {
    readonly id: string;
    readonly name: string;
    readonly value: string;
}

const Checkbox = ({ styleName, type, ...props }: Props) =>
    <div styleName={['checkbox-input', styleName].join(' ')}>
        <input styleName="input" type="checkbox" {...props} />
        <span styleName="square" />
        <span styleName="checkmark">x</span>
    </div>;

export default CSSModules(Checkbox, styles, { allowMultiple: true });
