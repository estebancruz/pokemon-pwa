import { shallow } from 'enzyme';
import * as React from 'react';
import Checkbox from './checkbox-input';

describe('Checkbox Input ', () => {
    const mock = 'test';
    it('should render an x', () => {
        const renderedComponent = shallow(
            <Checkbox id={mock} name={mock} value={mock} />,
        );
        expect(renderedComponent.text()).toEqual('x');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <Checkbox id={mock} name={mock} value={mock} />,
        );
        expect(renderedComponent.prop('className')).toBeDefined();
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <Checkbox id={mock} name={mock} value={mock} />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
