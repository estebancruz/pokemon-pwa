import { storiesOf } from '@kadira/storybook';
import * as React from 'react';
import CheckboxInput from './checkbox-input';

const mock = 'test';

storiesOf('Input', module).add('Checkbox', () =>
    <div>
        <CheckboxInput id={mock} name={mock} value={mock} />
    </div>,
);
