import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./form-group.less');

interface Props extends React.HTMLProps<HTMLDivElement> {}

const FormGroup = ({ children, styleName }: Props) =>
    <div styleName="form-group">
        {children}
    </div>;

export default CSSModules(FormGroup, styles, { allowMultiple: true });
