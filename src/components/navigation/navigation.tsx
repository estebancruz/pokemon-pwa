import { Glyphicon } from 'components/graphics';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { Link } from 'react-router-dom';

const styles = require('./navigation.less');
const Navigation = () =>
    <ul styleName="nav">
        <li role="presentation">
            <Link styleName="back-button" to={`/`}>
                <Glyphicon menuLeft />
            </Link>
        </li>
    </ul>;

export default CSSModules(Navigation, styles, { allowMultiple: true });
