import { shallow } from 'enzyme';
import * as React from 'react';
import Navigation from './navigation';

describe('Navigation', () => {
    it('should render', () => {
        const renderedComponent = shallow(<Navigation />);
        expect(renderedComponent.type()).toEqual('ul');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<Navigation />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
