import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./hidden.less');

interface Props extends React.HTMLProps<HTMLDivElement> {
    readonly until?: boolean;
}

const Hidden = ({ styleName, until, children, ...props }: Props) => {
    const visible = until ? 'visible' : '';
    return (
        <div styleName={['hidden', visible].join(' ')}>
            {children}
        </div>
    );
};

export default CSSModules(Hidden, styles, { allowMultiple: true });
