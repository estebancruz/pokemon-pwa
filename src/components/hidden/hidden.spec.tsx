import { shallow } from 'enzyme';
import * as React from 'react';
import Hidden from './hidden';

describe('Hidden', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render an outer div', () => {
        const renderedComponent = shallow(<Hidden id={mock} until />);
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop with the visible class added by default', () => {
        const renderedComponent = shallow(<Hidden id={mock} until />);
        expect(renderedComponent.prop('className')).toEqual('hidden visible');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<Hidden id={mock} until />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Hidden id={mock} until>
                {children}
            </Hidden>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
