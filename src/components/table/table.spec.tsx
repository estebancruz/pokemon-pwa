import { shallow } from 'enzyme';
import * as React from 'react';
import Table from './table';

describe('Component', () => {
    const mock: ReadonlyArray<string> = [
        '1',
        'one',
        '2',
        'two',
        '3',
        'three',
        '4',
        'four',
    ];
    const mockHeaders: ReadonlyArray<string> = ['label', 'value'];
    it('should render', () => {
        const renderedComponent = shallow(
            <Table data={mock} headers={mockHeaders} />,
        );
        expect(renderedComponent.type()).toEqual('table');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <Table data={mock} headers={mockHeaders} />,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
