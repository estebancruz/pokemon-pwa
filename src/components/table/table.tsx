import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./table.less');

interface Props {
    readonly data: ReadonlyArray<string>;
    readonly headers: ReadonlyArray<string>;
}

const Table = ({ data, headers }: Props) =>
    <table>
        <thead>
            <tr>
                {headers.map((th: string, idx: number) =>
                    <th key={`header_${idx}`}>
                        {th}
                    </th>,
                )}
            </tr>
        </thead>
        <tbody>
            {data.map((item, idx) =>
                <tr key={`tr_${idx}`}>
                    {headers.map((key: string, indx: number) =>
                        <td key={`td_${indx}`}>
                            {item[key]}
                        </td>,
                    )}
                </tr>,
            )}
        </tbody>
    </table>;

export default CSSModules(Table, styles);
