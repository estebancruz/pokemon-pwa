import { shallow } from 'enzyme';
import * as React from 'react';
import LoadingIndicator from './full-screen-loading-indicator';

describe('LoadingIndicator', () => {
    it('should render an outer div', () => {
        const renderedComponent = shallow(<LoadingIndicator visible />);
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop with the visible class added by default', () => {
        const renderedComponent = shallow(<LoadingIndicator visible />);
        expect(renderedComponent.prop('className')).toEqual('bg visible');
    });

    it('should not have the visible class without the visible prop', () => {
        const renderedComponent = shallow(<LoadingIndicator />);
        expect(renderedComponent.prop('className')).toEqual('bg');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<LoadingIndicator visible />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
