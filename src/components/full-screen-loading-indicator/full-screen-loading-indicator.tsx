import { Glyphicon } from 'components/graphics';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./full-screen-loading-indicator.less');

interface Props {
    readonly visible?: boolean;
}

const LoadingIndicator = (props: Props) =>
    <div styleName={['bg', props.visible ? 'visible' : ''].join(' ')}>
        <h1>
            <Glyphicon cog spin />
            &nbsp;Loading&nbsp;
            <Glyphicon cog spin />
        </h1>
    </div>;

export default CSSModules(LoadingIndicator, styles, { allowMultiple: true });
