import { storiesOf } from '@kadira/storybook';
import * as React from 'react';
import LoadingIndicator from './full-screen-loading-indicator';

storiesOf('Organisms', module).add('Full Screen Loading Indicator', () =>
    <div>
        <LoadingIndicator visible />
    </div>,
);
