import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./atomic-grid.less');

interface Props extends React.HTMLProps<HTMLDivElement> {}
const Row = ({ children, ...props }: Props) =>
    <div styleName="row" {...props}>
        {children}
    </div>;

export default CSSModules(Row, styles, { allowMultiple: true });
