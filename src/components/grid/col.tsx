import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { splitProps } from 'utils';
const styles = require('./atomic-grid.less');

enum ColProps {
    xs = 'xs',
    sm = 'sm',
    md = 'md',
    lg = 'lg',
    xsOffset = 'xsOffset',
    smOffset = 'smOffset',
    mdOffset = 'mdOffset',
    lgOffset = 'lgOffset',
    xsPull = 'xsPull',
    smPull = 'smPull',
    mdPull = 'mdPull',
    lgPull = 'lgPull',
    xsPush = 'xsPush',
    smPush = 'smPush',
    mdPush = 'mdPush',
    lgPush = 'lgPush',
    xsHidden = 'xsHidden',
    smHidden = 'smHidden',
    mdHidden = 'mdHidden',
    lgHidden = 'lgHidden',
}

interface Props extends React.HTMLProps<HTMLDivElement> {
    readonly xs?: number;
    readonly sm?: number;
    readonly md?: number;
    readonly lg?: number;

    readonly xsOffset?: number;
    readonly smOffset?: number;
    readonly mdOffset?: number;
    readonly lgOffset?: number;

    readonly xsPull?: number;
    readonly smPull?: number;
    readonly mdPull?: number;
    readonly lgPull?: number;

    readonly xsPush?: number;
    readonly smPush?: number;
    readonly mdPush?: number;
    readonly lgPush?: number;

    readonly xsHidden?: boolean;
    readonly smHidden?: boolean;
    readonly mdHidden?: boolean;
    readonly lgHidden?: boolean;
}

const sizeReducer = (sizes: ReadonlyArray<string>, props: Props) => (
    classNameArray: ReadonlyArray<string>,
    key: string,
) => {
    const size = sizes.reduce((s, next) => (next === key ? next : s), '');
    return size && size.length
        ? [...classNameArray, `col-${size.toLowerCase()}-${props[key]}`]
        : [...classNameArray];
};

const pushPullOffsetReducer = (
    modifiers: ReadonlyArray<string>,
    props: Props,
) => (classNameArray: ReadonlyArray<string>, key: string) => {
    const modifier = modifiers.reduce(
        (m, next) => (key.indexOf(next) > -1 ? next : m),
        '',
    );
    return modifier && modifier.length
        ? [
              ...classNameArray,
              `col-${key.replace(
                  modifier,
                  '',
              )}-${modifier.toLowerCase()}-${props[key]}`,
          ]
        : [...classNameArray];
};

const hiddenReducer = (props: Props) => (
    classNameArray: ReadonlyArray<string>,
    key: string,
) => {
    return key.indexOf('Hidden') > -1 && props[key] === true
        ? [...classNameArray, `hidden-${key.replace('Hidden', '')}`]
        : [...classNameArray];
};

const Col = ({ children, ...props }: Props) => {
    const [bsProps, otherProps] = splitProps(props, ColProps);
    const propKeys = Object.keys(bsProps);

    const sizes: ReadonlyArray<string> = propKeys.reduce(
        sizeReducer(['xs', 'sm', 'md', 'lg'], bsProps),
        [],
    );

    const pushPullOffset: ReadonlyArray<string> = propKeys.reduce(
        pushPullOffsetReducer(['Offset', 'Push', 'Pull'], bsProps),
        [],
    );

    const hiddens: ReadonlyArray<string> = propKeys.reduce(
        hiddenReducer(bsProps),
        [],
    );

    const styleNames: ReadonlyArray<any> = [
        ...sizes,
        ...pushPullOffset,
        ...hiddens,
    ];

    return (
        <div styleName={styleNames.join(' ')} {...otherProps}>
            {children}
        </div>
    );
};

export default CSSModules(Col, styles, { allowMultiple: true });
