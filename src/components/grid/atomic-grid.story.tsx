import { storiesOf } from '@kadira/storybook';
import * as React from 'react';
import Col from './col';
import Container from './container';
import Row from './row';

storiesOf('Grid', module).add('Container, Container fluid, Row and Col-*', () =>
    <div>
        <Container style={{ background: '#eee', margin: '20px' }}>
            <h4>This is a container</h4>
            <Row style={{ background: '#ccc' }}>
                <h4>This is a row</h4>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xs={3}
                    sm={3}
                    md={3}
                    lg={3}
                >
                    col-xs-3 col-sm-3 col-md-3 col-lg-3
                </Col>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xs={3}
                    sm={3}
                    md={3}
                    lg={3}
                >
                    col-xs-3 col-sm-3 col-md-3 col-lg-3
                </Col>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xs={3}
                    sm={3}
                    md={3}
                    lg={3}
                >
                    col-xs-3 col-sm-3 col-md-3 col-lg-3
                </Col>
            </Row>
        </Container>
        <Container fluid style={{ background: '#eee' }}>
            <h4>This is a fluid container</h4>
            <Row style={{ background: '#ccc' }}>
                <h4>This is a row</h4>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xs={3}
                    sm={3}
                    md={3}
                    lg={3}
                >
                    col-xs-3 col-sm-3 col-md-3 col-lg-3
                </Col>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xs={3}
                    sm={3}
                    md={3}
                    lg={3}
                >
                    col-xs-3 col-sm-3 col-md-3 col-lg-3
                </Col>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xs={3}
                    sm={3}
                    md={3}
                    lg={3}
                >
                    col-xs-3 col-sm-3 col-md-3 col-lg-3
                </Col>
            </Row>
            <Row style={{ background: '#cacaca' }}>
                <h4>
                    This is a row with col offset xs - view width (less than
                    768px)
                </h4>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xsOffset={3}
                >
                    col-xs-3-offset
                </Col>
            </Row>
            <Row style={{ background: '#cacaca' }}>
                <h4>
                    This is a row with col pull and pull - md view width
                    (desktops, 992px and up)
                </h4>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    md={7}
                    mdPush={3}
                >
                    col-md-7 col-md-3-push
                </Col>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    md={3}
                    mdPull={7}
                >
                    col-md-3 col-md-7-pull
                </Col>
            </Row>
            <Row style={{ background: '#cacaca' }}>
                <h4>
                    This is a row with col hidden - xs view width (less than
                    768px)
                </h4>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xs={3}
                    sm={3}
                    md={3}
                    lg={3}
                >
                    col-xs-3 col-sm-3 col-md-3 col-lg-3
                </Col>
                <Col
                    style={{ background: '#e3e3e3', border: '1px solid #000' }}
                    xsHidden
                >
                    col-xs-3
                </Col>
            </Row>
        </Container>
    </div>,
);
