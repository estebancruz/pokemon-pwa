import { shallow } from 'enzyme';
import * as React from 'react';

import Row from './row';

describe('Row', () => {
    const children = <p>Test</p>;
    it('should render an outer div', () => {
        const renderedComponent = shallow(
            <Row>
                {children}
            </Row>,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <Row>
                {children}
            </Row>,
        );
        expect(renderedComponent.prop('className')).toEqual('row');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <Row>
                {children}
            </Row>,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Row>
                {children}
            </Row>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
