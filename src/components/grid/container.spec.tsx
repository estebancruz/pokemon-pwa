import { shallow } from 'enzyme';
import * as React from 'react';

import Container from './container';

describe('Container', () => {
    const children = <p>Test</p>;
    it('should render an outer div', () => {
        const renderedComponent = shallow(
            <Container>
                {children}
            </Container>,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <Container>
                {children}
            </Container>,
        );
        expect(renderedComponent.prop('className')).toEqual('container');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <Container>
                {children}
            </Container>,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Container>
                {children}
            </Container>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
