import * as React from 'react';
import * as CSSModules from 'react-css-modules';
const styles = require('./atomic-grid.less');

interface Props extends React.HTMLProps<HTMLDivElement> {
    readonly fluid?: boolean;
}

const Container = ({ children, fluid, ...props }: Props) =>
    <div styleName={fluid ? 'container-fluid' : 'container'} {...props}>
        {children}
    </div>;

export default CSSModules(Container, styles);
