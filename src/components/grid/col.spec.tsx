import { shallow } from 'enzyme';
import * as React from 'react';
import Col from './col';

describe('Col', () => {
    const children = <p>Test</p>;
    it('should render an outer div', () => {
        const renderedComponent = shallow(
            <Col xs={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a col-xs-3 className prop', () => {
        const renderedComponent = shallow(
            <Col xs={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('col-xs-3');
    });

    it('should have a col-sm-3 className prop', () => {
        const renderedComponent = shallow(
            <Col sm={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('col-sm-3');
    });

    it('should have a col-xs-3-offset className prop', () => {
        const renderedComponent = shallow(
            <Col xsOffset={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('col-xs-offset-3');
    });

    it('should have a col-xs-3-push className prop', () => {
        const renderedComponent = shallow(
            <Col xsPush={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('col-xs-push-3');
    });

    it('should have a col-xs-3-pull className prop', () => {
        const renderedComponent = shallow(
            <Col xsPull={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('col-xs-pull-3');
    });

    it('should have a xs-hidden className prop', () => {
        const renderedComponent = shallow(
            <Col xsHidden>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('hidden-xs');
    });

    it('should have a sm-hidden className prop', () => {
        const renderedComponent = shallow(
            <Col smHidden>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('hidden-sm');
    });

    it('should have a md-hidden className prop', () => {
        const renderedComponent = shallow(
            <Col mdHidden>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('hidden-md');
    });

    it('should have a lg-hidden className prop', () => {
        const renderedComponent = shallow(
            <Col lgHidden>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual('hidden-lg');
    });

    it('should have more than one col className prop', () => {
        const renderedComponent = shallow(
            <Col xs={3} sm={4} md={4} lg={12}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual(
            'col-xs-3 col-sm-4 col-md-4 col-lg-12',
        );
    });

    it('should have more than one modifier className prop', () => {
        const renderedComponent = shallow(
            <Col xsOffset={3} xsPush={3} xsPull={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('className')).toEqual(
            'col-xs-offset-3 col-xs-push-3 col-xs-pull-3',
        );
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <Col xs={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Col xs={3}>
                {children}
            </Col>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
