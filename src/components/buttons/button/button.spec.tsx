import { shallow } from 'enzyme';
import * as React from 'react';
import Button from './button';

describe('Button', () => {
    const children = <p>Test</p>;
    it('should render', () => {
        const renderedComponent = shallow(
            <Button>
                {children}
            </Button>,
        );
        expect(renderedComponent.type()).toEqual('button');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(
            <Button>
                {children}
            </Button>,
        );
        expect(renderedComponent.prop('className')).toEqual('btn');
    });

    it('should have a primary className added', () => {
        const renderedComponent = shallow(
            <Button primary>
                {children}
            </Button>,
        );
        expect(renderedComponent.prop('className')).toEqual('btn btn-primary');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(
            <Button>
                {children}
            </Button>,
        );
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Button>
                {children}
            </Button>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
