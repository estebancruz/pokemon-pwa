import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { splitProps } from 'utils';

const styles = require('./button.less');

interface BootstrapModifiers {
    readonly default?: boolean;

    /**
     * Used for submit buttons and other CTAs
     */
    readonly primary?: boolean;

    readonly success?: boolean;
    readonly info?: boolean;
    readonly warning?: boolean;
    readonly danger?: boolean;
}

type BootstrapButton = React.HTMLProps<HTMLButtonElement> & BootstrapModifiers;

enum Modifiers {
    default = 'default',
    primary = 'primary',
    success = 'success',
    info = 'info',
    warning = 'warning',
    danger = 'danger',
}

const styleize = (prefix: string, props: BootstrapButton): string =>
    Object.keys(props).reduce((cssClass, key) => {
        cssClass +=
            Object.keys(Modifiers).indexOf(key) > -1 && props[key] === true
                ? ` ${prefix}-${key}`
                : '';

        return cssClass;
    }, prefix);

interface Props extends BootstrapButton {}
const PrimaryButton = ({ children, ...props }: Props): JSX.Element => {
    const [bsProps, otherProps] = splitProps(props, Modifiers);
    return (
        <button styleName={styleize('btn', bsProps)} {...otherProps}>
            {children}
        </button>
    );
};
export default CSSModules(PrimaryButton, styles, { allowMultiple: true });
