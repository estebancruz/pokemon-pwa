import { storiesOf } from '@kadira/storybook';
import * as React from 'react';
import Button from './button';

storiesOf('Buttons', module).add('Bootstrap', () =>
    <div>
        <p>
            <Button onClick={() => 'woop'} default>
                default button
            </Button>
        </p>
        <p>
            <Button onClick={() => 'woop'} disabled>
                disabled button
            </Button>
        </p>
        <p>
            <Button onClick={() => 'woop'} primary>
                primary button
            </Button>
        </p>
        <p>
            <Button onClick={() => 'woop'} success>
                success button
            </Button>
        </p>
        <p>
            <Button onClick={() => 'woop'} info>
                info button
            </Button>
        </p>
        <p>
            <Button onClick={() => 'woop'} warning>
                warning button
            </Button>
        </p>
        <p>
            <Button onClick={() => 'woop'} danger>
                danger button
            </Button>
        </p>
    </div>,
);
