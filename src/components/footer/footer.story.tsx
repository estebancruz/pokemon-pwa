import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import Footer from './footer';

storiesOf('Organisms', module).add('Footer', () => <Footer />);
