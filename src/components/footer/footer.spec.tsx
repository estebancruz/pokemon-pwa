import { shallow } from 'enzyme';
import * as React from 'react';
import Footer from './footer';

describe('Footer', () => {
    it('should render an outer div', () => {
        const renderedComponent = shallow(<Footer />);
        expect(renderedComponent.type()).toEqual('div');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(<Footer />);
        expect(renderedComponent.prop('className')).toEqual('footer');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<Footer />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
