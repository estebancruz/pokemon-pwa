import { Logo } from 'components/graphics';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { Link } from 'react-router-dom';

const styles = require('./footer.less');

const Footer = () =>
    <div styleName="footer">
        <div styleName="footerContentContainer">
            <Link styleName="logoContainer" to={'/'}>
                <Logo />
            </Link>
            <div styleName="footerBottomLinks">
                <p styleName="copyright">
                    Copyright &copy; 2017 All Rights Reserved
                </p>
            </div>
        </div>
    </div>;

export default CSSModules(Footer, styles);
