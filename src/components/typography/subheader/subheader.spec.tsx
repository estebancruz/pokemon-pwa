import { shallow } from 'enzyme';
import * as React from 'react';
import Subheader from './subheader';

describe('Subheader', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render an Subheader', () => {
        const renderedComponent = shallow(<Subheader>Test</Subheader>);
        expect(renderedComponent.text()).toEqual('Test');
    });

    it('should have an extra className prop', () => {
        const renderedComponent = shallow(<Subheader styleName={mock} />);
        expect(renderedComponent.prop('className')).toEqual('test');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<Subheader />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Subheader styleName={mock}>
                {children}
            </Subheader>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
