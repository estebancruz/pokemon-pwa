import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import SubHeader from './subheader';

storiesOf('Typography', module).add('Subheader', () =>
    <SubHeader warmBlack>This is a subheader</SubHeader>,
);
