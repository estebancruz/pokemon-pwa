import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { optionalize } from 'components/utility';

const styles = require('./subheader.less');

interface Props extends React.HTMLProps<HTMLHeadingElement> {
    readonly italic?: boolean;
    readonly noMargin?: boolean;
    readonly small?: boolean;
    readonly big?: boolean;

    // Color
    readonly warmBlack?: boolean;
    readonly lightBrown?: boolean;
}

const Subheader = ({
    children,
    italic,
    noMargin,
    warmBlack,
    lightBrown,
    small,
    big,
    ...props,
}: Props) =>
    <h2
        styleName={[
            'subheader',
            ...optionalize({
                big,
                italic,
                lightBrown,
                noMargin,
                small,
                warmBlack,
            }),
        ].join(' ')}
        {...props}
    >
        {children}
    </h2>;

export default CSSModules(Subheader, styles, { allowMultiple: true });
