import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import { optionalize } from 'components/utility';

const styles = require('./form-title.less');

interface Props extends React.HTMLProps<HTMLHeadingElement> {}

const FormTitle = ({ children, styleName, ...props }: Props) =>
    <h4
        styleName={['form-title', styleName, ...optionalize({})].join(' ')}
        {...props}
    >
        {children}
    </h4>;

export default CSSModules(FormTitle, styles, { allowMultiple: true });
