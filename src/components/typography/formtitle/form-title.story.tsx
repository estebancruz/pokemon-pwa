import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import FormTitle from './form-title';

storiesOf('Typography', module).add('Form Title', () =>
    <FormTitle>Form Title</FormTitle>,
);
