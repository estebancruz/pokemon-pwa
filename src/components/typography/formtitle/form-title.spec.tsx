import { shallow } from 'enzyme';
import * as React from 'react';
import FormTitle from './form-title';

describe('FormTitle', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render a FormTitle with text', () => {
        const renderedComponent = shallow(<FormTitle>Test</FormTitle>);
        expect(renderedComponent.text()).toEqual('Test');
    });

    it('should have an extra className prop', () => {
        const renderedComponent = shallow(<FormTitle styleName={mock} />);
        expect(renderedComponent.prop('className')).toEqual('form-title test');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<FormTitle />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <FormTitle styleName={mock}>
                {children}
            </FormTitle>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
