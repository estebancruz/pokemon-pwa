import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import InternalLink from './internal-link';

storiesOf('Typography', module).add('Internal Link', () =>
    <InternalLink to="test">Lorem Ipsum Los Corazones</InternalLink>,
);
