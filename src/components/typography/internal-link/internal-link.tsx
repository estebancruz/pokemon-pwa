import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { Link, LinkProps } from 'react-router-dom';

const styles = require('./internal-link.less');

interface Props extends LinkProps {}

const InternalLink = ({ styleName, to, children, ...props }: Props) =>
    <Link {...props} styleName={[styleName, 'link'].join(' ')} to={to}>
        {children}
    </Link>;

export default CSSModules(InternalLink, styles, { allowMultiple: true });
