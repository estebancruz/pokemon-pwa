import { shallow } from 'enzyme';
import * as React from 'react';
import InternalLink from './internal-link';

describe('InternalLink', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render an InternalLink', () => {
        const renderedComponent = shallow(
            <InternalLink to={mock}>Test</InternalLink>,
        );
        expect(renderedComponent.text()).toEqual('<Link />');
    });

    it('should have an extra className prop', () => {
        const renderedComponent = shallow(
            <InternalLink to={mock} styleName={mock} />,
        );
        expect(renderedComponent.prop('className')).toEqual('test link');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<InternalLink to={mock} />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <InternalLink to={mock}>
                {children}
            </InternalLink>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
