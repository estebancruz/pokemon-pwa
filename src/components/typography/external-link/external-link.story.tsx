import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import ExternalLink from './external-link';

storiesOf('Typography', module).add('External Link', () =>
    <ExternalLink href="test">Lorem Ipsum Los Corazones</ExternalLink>,
);
