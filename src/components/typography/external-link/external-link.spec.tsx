import { shallow } from 'enzyme';
import * as React from 'react';
import ExternalLink from './external-link';

describe('ExternalLink', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render an <ExternalLink> tag with text', () => {
        const renderedComponent = shallow(
            <ExternalLink href={mock}>Test</ExternalLink>,
        );
        expect(renderedComponent.text()).toEqual('Test');
    });

    it('should have a className prop', () => {
        const renderedComponent = shallow(<ExternalLink href={mock} />);
        expect(renderedComponent.prop('className')).toBeDefined();
    });

    it('should have a default className prop', () => {
        const renderedComponent = shallow(<ExternalLink href={mock} />);
        expect(renderedComponent.prop('className')).toEqual('link');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<ExternalLink href={mock} />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <ExternalLink href={mock}>
                {children}
            </ExternalLink>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
