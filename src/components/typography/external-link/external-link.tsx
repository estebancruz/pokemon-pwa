import { optionalize } from 'components/utility';
import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./external-link.less');

interface Props extends React.HTMLProps<HTMLAnchorElement> {
    readonly href: string;
    readonly white?: boolean;
}

const ExternalLink = ({ white, styleName, children, ...props }: Props) =>
    <a
        {...props}
        styleName={[styleName, 'link', ...optionalize({ white })].join(' ')}
        target="_blank"
    >
        {children}
    </a>;

export default CSSModules(ExternalLink, styles, { allowMultiple: true });
