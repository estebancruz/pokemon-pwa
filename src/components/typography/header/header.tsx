import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./header.less');

interface Props extends React.HTMLProps<HTMLHeadingElement> {}

const Header = ({ children, styleName, ...props }: Props) =>
    <h1 styleName={['headline'].join(' ')} {...props}>
        {children}
    </h1>;

export default CSSModules(Header, styles, { allowMultiple: true });
