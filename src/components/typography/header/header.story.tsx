import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import Header from './header';

storiesOf('Typography', module).add('Headline', () =>
    <Header>This is a headline</Header>,
);
