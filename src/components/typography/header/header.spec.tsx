import { shallow } from 'enzyme';
import * as React from 'react';
import Header from './header';

describe('Header', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render a Header with text', () => {
        const renderedComponent = shallow(<Header>Test</Header>);
        expect(renderedComponent.text()).toEqual('Test');
    });

    it('should have an extra className prop', () => {
        const renderedComponent = shallow(<Header styleName={mock} />);
        expect(renderedComponent.prop('className')).toEqual('headline test');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<Header />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <Header styleName={mock}>
                {children}
            </Header>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
