import * as React from 'react';

import { storiesOf } from '@kadira/storybook';
import BodyCopy from './bodycopy';

storiesOf('Typography', module).add('Body Copy', () =>
    <BodyCopy>Lorem Ipsum Los Corazones</BodyCopy>,
);
