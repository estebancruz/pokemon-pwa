import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./bodycopy.less');

interface Props extends React.HTMLProps<HTMLParagraphElement> {}
const BodyCopy = ({ children, ...props }: Props): JSX.Element =>
    <p styleName="bodycopy" {...props}>
        {children}
    </p>;
export default CSSModules(BodyCopy, styles, { allowMultiple: true });
