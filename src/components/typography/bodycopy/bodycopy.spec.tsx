import { shallow } from 'enzyme';
import * as React from 'react';
import BodyCopy from './bodycopy';

describe('BodyCopy Input ', () => {
    const mock = 'test';
    const children = <p>Test</p>;
    it('should render an <BodyCopy> tag', () => {
        const renderedComponent = shallow(<BodyCopy>Test</BodyCopy>);
        expect(renderedComponent.text()).toEqual('Test');
    });

    it('should have an extra className prop', () => {
        const renderedComponent = shallow(<BodyCopy styleName={mock} />);
        expect(renderedComponent.prop('className')).toEqual('test');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<BodyCopy />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });

    it('should have children', () => {
        const renderedComponent = shallow(
            <BodyCopy styleName={mock}>
                {children}
            </BodyCopy>,
        );
        expect(renderedComponent.contains(children)).toEqual(true);
    });
});
