import { PhoneNumberFormat, PhoneNumberUtil } from 'google-libphonenumber';

namespace Parsers {
    const phoneUtil = PhoneNumberUtil.getInstance();

    export namespace Validators {
        export const isPhoneNumber = (val: string): string => {
            try {
                if (!!phoneUtil.parse(val, 'US') && val.length >= 10) {
                    return '';
                } else {
                    return 'Please enter a valid phone number.';
                }
            } catch (e) {
                return 'Please enter a valid phone number.';
            }
        };

        export const notEmpty = (val: string): string => {
            return val && val.trim() ? '' : 'This field cannot be left blank.';
        };

        export const nonNumeric = (val: string): string =>
            /^([^0-9]*)$/.exec(val) ? '' : 'Numbers are not allowed.';
    }

    export namespace Formatters {
        export const formatPhoneNumber = (val: string): string => {
            try {
                return phoneUtil.format(
                    phoneUtil.parse(val, 'US'),
                    PhoneNumberFormat.NATIONAL,
                );
            } catch (e) {
                return val;
            }
        };

        export const capitalizeFirstLetterOfEachWord = (
            val: string,
        ): string => {
            try {
                return val
                    .split(' ')
                    .map(word => word[0].toUpperCase() + word.slice(1))
                    .join(' ');
            } catch (e) {
                return val;
            }
        };
    }
}

export default Parsers;
