const optionalize = (options: {
    readonly [k: string]: boolean | undefined;
}): ReadonlyArray<string> => {
    return Object.keys(options).map(k => (options[k] === true ? k : ''));
};

export default optionalize;
