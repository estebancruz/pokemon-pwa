import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./textual-list.less');

interface Props {
    readonly list: ReadonlyArray<string>;
}

const TextualList = ({ list }: Props) =>
    <ul styleName="textual-list">
        {list.map((item, idx) =>
            <li key={`$item_${idx}`}>
                {item}
            </li>,
        )}
    </ul>;

export default CSSModules(TextualList, styles);
