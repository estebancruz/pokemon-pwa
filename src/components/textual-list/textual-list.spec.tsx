import { shallow } from 'enzyme';
import * as React from 'react';
import TextualList from './textual-list';

describe('Component', () => {
    const mock: ReadonlyArray<string> = [
        '1',
        'one',
        '2',
        'two',
        '3',
        'three',
        '4',
        'four',
    ];
    it('should render', () => {
        const renderedComponent = shallow(<TextualList list={mock} />);
        expect(renderedComponent.type()).toEqual('ul');
    });

    it('should not adopt an invalid prop', () => {
        const renderedComponent = shallow(<TextualList list={mock} />);
        expect(renderedComponent.prop('random')).toBeUndefined();
    });
});
